/*
** class.h for server in /home/bousca_a/rendu/epic_js_fantasy
**
** Made by Antonin Bouscarel
** Login   <bousca_a@epitech.net>
**
** Started on  Sat May 10 05:40:34 2014 Antonin Bouscarel
** Last update Sun May 11 00:37:27 2014 camill_n
*/

#ifndef CLASS_H_
# define CLASS_H_

# include "server.h"

t_class		*get_class_ptr(t_data *data, char *class_name);
void		add_class_attribute(t_data *data, char *class_name,
				    int macro, int value);
void		add_class(t_data *data, char *name);
void		del_class(t_data *data, t_class *class);
void		add_adv_to_class(t_data *data, char *name_class,
				 char *adv_name);
void		add_adv_to_class(t_data *, char *, char *);
void		add_class_to_entity(t_data *, char *, char *, t_room *);

#endif
