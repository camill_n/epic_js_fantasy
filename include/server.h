/*
** server.h for server in /home/camill_n/rendu/epic_js_fantasy
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sat May 10 01:44:14 2014 camill_n
** Last update Sat May 10 20:40:25 2014 Antonin Bouscarel
*/

#ifndef SERVER_H_
# define SERVER_H_

# include "adv.h"
# include "entity.h"
# include "object.h"
# include "room.h"
# include "data.h"
# include "init.h"
# include "utils.h"
# include "clients.h"
# include "wordtab.h"
# include "add_sample.h"
# include "display_sample.h"
# include "parse_sample.h"
# include "class.h"
# include "sock.h"
# include "action.h"

#endif
