/*
** display_sample.h for server in /home/bousca_a/rendu/epic_js_fantasy
**
** Made by Antonin Bouscarel
** Login   <bousca_a@epitech.net>
**
** Started on  Sat May 10 17:33:39 2014 Antonin Bouscarel
** Last update Sat May 10 17:35:16 2014 Antonin Bouscarel
*/

#ifndef DISPLAY_SAMPLE_H_
# define DISPLAY_SAMPLE_H_

void	display_class(t_data *data);
void	display_armor(t_data *data);
void	display_weapon(t_data *data);
void	display_adv(t_data *data);

#endif
