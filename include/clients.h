/*
** clients.h for server in /home/camill_n/rendu/epic_js_fantasy
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sat May 10 13:02:26 2014 camill_n
** Last update Sat May 10 17:31:14 2014 camill_n
*/

#ifndef CLIENTS_H_
# define CLIENTS_H_

typedef struct		s_client
{
  int			sock;
  t_sock_in		sock_in_client;
  t_entity		*entity;
  struct s_client	*next;
}			t_client;

void		add_client(t_data *data, int sock);
void		del_client(t_data *data, t_client *client);
void		set_entity_to_client(t_data *data, t_client *client, t_entity *entity);
t_client	*get_last_client(t_data *data);
void		check_client(t_data *data, fd_set *rdfs);

#endif
