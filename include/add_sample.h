/*
** add_sample.h for server in /home/bousca_a/rendu/epic_js_fantasy
**
** Made by Antonin Bouscarel
** Login   <bousca_a@epitech.net>
**
** Started on  Sat May 10 17:37:07 2014 Antonin Bouscarel
** Last update Sat May 10 18:04:18 2014 Antonin Bouscarel
*/

#ifndef ADD_SAMPLE_H_
# define ADD_SAMPLE_H_

int	add_new_sample_class(t_data *data, char **line);
int	add_new_sample_weapon(t_data *data, char **line);
int	add_new_sample_armor(t_data *data, char **line);
int	add_new_sample_adv(t_data *data, char **line);

#endif
