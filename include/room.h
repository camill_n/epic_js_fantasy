/*
** room.h for server in /home/bousca_a/rush_rpg/include
**
** Made by Antonin Bouscarel
** Login   <bousca_a@epitech.net>
**
** Started on  Sat May 10 01:27:29 2014 Antonin Bouscarel
** Last update Sat May 10 14:05:31 2014 camill_n
*/

#ifndef ROOM_H_
# define ROOM_H_

typedef struct s_data	t_data;

typedef struct	s_room
{
  int		id_r;
  char		*name;
  int		nb_join;
  int		*join;
  t_entity	*entity;
  int		nb_entity[2];
  int		avd;
  struct s_room	*next;
}		t_room;

void	add_join(t_data *data, char *name_room, int id);
void	set_avd_room(t_data *data, char *name_room, int avd);
char	*get_room_name(t_data *data, int id_r);
void	add_room(t_data *data, char *name);
void	del_room(t_data *data, t_room *room);
t_room	*get_ptr_room(t_data *data, char *name);

#endif
