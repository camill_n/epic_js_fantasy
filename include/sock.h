/*
** sock.h for server in /home/camill_n/rendu/epic_js_fantasy
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sat May 10 15:27:01 2014 camill_n
** Last update Sat May 10 17:35:44 2014 camill_n
*/

#ifndef SOCK_H_
# define SOCK_H_

void	init_srv_socket(t_data *data);
void	create_client(t_data *data, int sock_srv, int *h_sock);
int	check_socket(int sockfd);
int	listen_loop(t_data *data);

#endif
