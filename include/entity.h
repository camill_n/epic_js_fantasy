/*
** entity.h for server in /home/bousca_a/rush_rpg/include
**
** Made by Antonin Bouscarel
** Login   <bousca_a@epitech.net>
**
** Started on  Sat May 10 01:31:25 2014 Antonin Bouscarel
** Last update Sun May 11 00:29:27 2014 Antonin Bouscarel
*/

#ifndef ENTITY_H_
# define ENTITY_H_

# define HP		1
# define SPE		2
# define SPEED		3
# define ATK		4
# define ATK_SPE	5
# define ATK_SPE_TYPE	6
# define BUFF_SPEED	7
# define BUFF_ATK	8
# define SPE_COST	9

typedef struct	s_weapon	t_weapon;
typedef struct	s_armor		t_armor;
typedef struct	s_room		t_room;
typedef struct	s_data		t_data;
typedef struct	s_adv		t_adv;

typedef struct		s_class
{
  char			name[50];
  int			atk_spe;
  int			nb_avg;
  t_adv			*adv;
  int			atk_spe_type; //0 = Damage | 1 = heal
  int			buff_speed;
  int			buff_deg;
  int			spe_cost;
  struct s_class	*next;
}			t_class;

typedef struct		s_entity
{
  int			id_e;
  int			type_e;
  char			*name;
  t_class		*class;
  int			hp;
  int			spe;
  int			speed;
  int			atk;
  t_weapon		*weapon;
  t_armor		*armor;
  struct s_entity	*next;
}			t_entity;

void			add_entity(t_data *data, char *name_room, int type_e, char *name);
void			add_entity_attributes(t_data *data, char *name_room,
					      int macro, int value);
void			del_entity(t_room *room, t_entity *entity);
t_entity		*get_entity_addr(t_data *data, t_room *room,
					 char *name_entity);
void			copy_entity(t_data *data, t_room *dest,
				    t_entity *entity);
void			set_entity_attr(t_data *data, t_entity *entity,
					t_room *room);

#endif
