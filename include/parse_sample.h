/*
** parse_sample.h for server in /home/bousca_a/rendu/epic_js_fantasy/include
**
** Made by Antonin Bouscarel
** Login   <bousca_a@epitech.net>
**
** Started on  Sat May 10 16:24:55 2014 Antonin Bouscarel
** Last update Sat May 10 18:03:29 2014 Antonin Bouscarel
*/

#ifndef PARSE_SAMPLE_
# define PARSE_SAMPLE_

# define PCLASS		".class"
# define PWEAPON	".weapon"
# define PARMOR		".armor"
# define PADV		".adv"

int		add_data_sample(t_data *data, char *line);
void		parse_sample_file(t_data *data);

#endif
