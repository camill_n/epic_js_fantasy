/*
** adv.h for server in /home/bousca_a/rush_rpg/include
**
** Made by Antonin Bouscarel
** Login   <bousca_a@epitech.net>
**
** Started on  Sat May 10 01:42:37 2014 Antonin Bouscarel
** Last update Sat May 10 17:13:45 2014 Antonin Bouscarel
*/

#ifndef ADV_H_
# define ADV_H_

# include "server.h"

# define BONUS_ARM	1
# define BONUS_ATK	2
# define BONUS_SPEED	3
# define BONUS_WEAPON	4
# define BONUS_SPE	5

typedef struct	s_data	t_data;

typedef struct	s_adv
{
  char		name[50];
  int		bonus_arm;
  int		bonus_atk;
  int		bonus_spe;
  int		bonus_speed;
  int		bonus_weapon;
  struct s_adv	*next;
}		t_adv;

t_adv		*get_adv_ptr(t_data *data, char *adv_name);
void		add_adv_attribute(t_data *data, char *adv_name,
				  int macro, int value);
void		add_adv(t_data *data, char *name);
void		del_adv(t_data *data, t_adv *adv);

#endif
