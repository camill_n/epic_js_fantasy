/*
** action.h for server in /home/bousca_a/rendu/epic_js_fantasy
**
** Made by Antonin Bouscarel
** Login   <bousca_a@epitech.net>
**
** Started on  Sat May 10 20:36:26 2014 Antonin Bouscarel
** Last update Sat May 10 20:38:27 2014 Antonin Bouscarel
*/

#ifndef ACTION_H_
# define ACTION_H_

void		new_basic_battle(t_data *data, char *name_entity_src,
				 char *name_entity_dest, t_room *room);
void		new_spe_battle(t_data *data, char *name_entity_src,
			       char *name_entity_dest, t_room *room);
void		change_room(t_data *data, char *name_entity, char *src,
			    char *dest);

#endif
