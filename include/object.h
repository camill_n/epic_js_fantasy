/*
** object.h for server in /home/bousca_a/rush_rpg/include
**
** Made by Antonin Bouscarel
** Login   <bousca_a@epitech.net>
**
** Started on  Sat May 10 01:38:25 2014 Antonin Bouscarel
** Last update Sat May 10 06:35:47 2014 Antonin Bouscarel
*/

#ifndef OBJECT_H_
# define OBJECT_H_

# define DEG		1
# define ARMOR_BONUS	2

typedef struct		s_weapon
{
  char			name[50];
  int			deg;
  struct s_weapon	*next;
}			t_weapon;

typedef struct		s_armor
{
  char			name[50];
  int			bonus;
  struct s_armor	*next;
}			t_armor;

typedef struct		s_obj
{
  t_weapon		*weapon;
  t_armor		*armor;
  t_class		class[2];
}			t_obj;

t_weapon		*get_weapon_ptr(t_data *data, char *weapon_name);
void			add_weapon_attribute(t_data *data, char *weapon_name,
					     int macro, int value);
void			add_weapon(t_data *data, char *name);
void			del_weapon(t_data *data, t_weapon *weapon);
t_armor			*get_armor_ptr(t_data *data, char *armor_name);
void			add_armor_attribute(t_data *data, char *armor_name,
					    int macro, int value);
void			add_armor(t_data *data, char *name);
void			del_armor(t_data *data, t_armor *armor);

#endif
