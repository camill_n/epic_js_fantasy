/*
** utils.h for server in /home/camill_n/rendu/epic_js_fantasy
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sat May 10 02:56:15 2014 camill_n
** Last update Sat May 10 13:19:55 2014 camill_n
*/

#ifndef UTILS_H_
# define UTILS_H_

void		disp_entity(t_data *data, char *name_room);
void		disp_room(t_data *data);
void		disp_client(t_data *data);

#endif
