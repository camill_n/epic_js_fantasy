/*
** data.h for fantasy_rpg in /home/bousca_a/rush_rpg/include
**
** Made by Antonin Bouscarel
** Login   <bousca_a@epitech.net>
**
** Started on  Sat May 10 01:24:53 2014 Antonin Bouscarel
** Last update Sat May 10 15:25:58 2014 camill_n
*/

#ifndef DATA_H_
# define DATA_H_

# define CLASS	data->sample->class
# define WEAPON	data->sample->weapon
# define ARMOR	data->sample->armor
# define ADV	data->sample->adv

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>

typedef struct s_client		t_client;
typedef struct sockaddr_in	t_sock_in;
typedef struct sockaddr		t_sock;

typedef struct	s_sample
{
  t_class	*class;
  t_weapon	*weapon;
  t_armor	*armor;
  t_adv		*adv;
}		t_sample;

typedef struct	s_data
{
  int		nb_room;
  t_room	*room;
  int		nb_client;
  int		nb_entity;
  t_client	*client;
  t_sample	*sample;
  t_sock_in	sock_in_srv;
  int		sock_srv;
}		t_data;

#endif
