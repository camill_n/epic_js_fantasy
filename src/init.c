/*
** init.c for server in /home/camill_n/rendu/epic_js_fantasy
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sat May 10 02:10:09 2014 camill_n
** Last update Sat May 10 13:13:31 2014 camill_n
*/

#include <stdlib.h>
#include "server.h"

void		init_data(t_data *data)
{
  data->nb_room = 0;
  data->nb_client = 0;
  data->nb_entity = 0;
  data->room = NULL;
  data->client = NULL;
  if ((data->sample = malloc(sizeof(t_sample))) == NULL)
    exit(0);
  //data->client = NULL;
}
