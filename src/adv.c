/*
** a.c for server in /home/camill_n/rendu/epic_js_fantasy/src
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sat May 10 01:42:30 2014 camill_n
** Last update Sat May 10 17:13:31 2014 Antonin Bouscarel
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "server.h"

t_adv		*get_adv_ptr(t_data *data, char *adv_name)
{
  t_adv	*tmp;

  tmp = ADV;
  while (tmp != NULL && strcmp(tmp->name, adv_name) != 0)
    tmp = tmp->next;
  return (tmp);
}

void		add_adv_attribute(t_data *data, char *adv_name, int macro, int value)
{
  t_adv	*adv;

  adv = get_adv_ptr(data, adv_name);
  macro == BONUS_ARM ? adv->bonus_arm = value : 0;
  macro == BONUS_ATK ? adv->bonus_atk = value : 0;
  macro == BONUS_SPEED ? adv->bonus_speed = value : 0;
  macro == BONUS_WEAPON ? adv->bonus_weapon = value : 0;
  macro == BONUS_SPE ? adv->bonus_spe = value : 0;
}

void		add_adv(t_data *data, char *name)
{
    t_adv	*tmp;
    t_adv	*new_adv;

    new_adv = malloc(sizeof(t_adv));
    memcpy(new_adv, name, 50);
    new_adv->next = NULL;
    if (data->sample->adv == NULL)
        data->sample->adv = new_adv;
    else
    {
        tmp = data->sample->adv;
        while (tmp->next != NULL)
            tmp = tmp->next;
        tmp->next = new_adv;
    }
}

void		del_adv(t_data *data, t_adv *adv)
{
    t_adv	*tmp;

    if (data->sample->adv != NULL && data->sample->adv->next == NULL)
      data->sample = NULL;
    else if (data->sample->adv == adv)
        data->sample->adv = adv->next;
    else
    {
        tmp = data->sample->adv;
        while (tmp->next != adv)
            tmp = tmp->next;
        if (adv->next != NULL)
            tmp->next = adv->next;
        else
            tmp->next = NULL;
    }
    free(adv);
}
