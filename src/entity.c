/*
** a.c for server in /home/camill_n/rendu/epic_js_fantasy/src
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sat May 10 01:42:30 2014 camill_n
** Last update Sun May 11 00:28:40 2014 Antonin Bouscarel
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "server.h"

void		set_entity_attr(t_data *data, t_entity *entity,
				t_room *room)
{
  t_entity	*tmp;

  tmp = room->entity;
  while (tmp != NULL)
    tmp = tmp->next;
  tmp = entity;
  tmp->next = NULL;
}

t_entity	*get_entity_addr(t_data *data, t_room *room, char *name_entity)
{
  t_entity	*tmp;

  tmp = room->entity;
  while (tmp->next != NULL && strcmp(tmp->name, name_entity) != 0)
    tmp = tmp->next;
  return (tmp);
}

void		add_entity_attributes(t_data *data, char *name_room,
				     int macro, int value)
{
  t_room	*room;
  t_entity	*tmp;

  room = get_ptr_room(data, name_room);
  tmp = room->entity;
  while (tmp != NULL && tmp->next != NULL)
    tmp = tmp->next;
  macro == HP ? tmp->hp = value : 0;
  macro == SPE ? tmp->spe = value : 0;
  macro == SPEED ? tmp->speed = value : 0;
  macro == ATK ? tmp->atk = value : 0;
}

void		add_entity(t_data *data, char *name_room,
			   int type_e, char *name)
{
  t_room	*room;
  t_entity	*tmp;
  t_entity	*new_entity;

  room = get_ptr_room(data, name_room);
  new_entity = malloc(sizeof(t_entity));
  new_entity->name = strdup(name);
  new_entity->id_e = data->nb_entity;
  new_entity->type_e = type_e;
  new_entity->spe = 0;
  new_entity->speed = 0;
  new_entity->atk = 0;
  new_entity->hp = 0;
  new_entity->next = NULL;
  if (room->entity == NULL)
    room->entity = new_entity;
  else
    {
      tmp = room->entity;
      while (tmp->next != NULL)
	tmp = tmp->next;
      tmp->next = new_entity;
    }
  room->nb_entity[type_e]++;
  ++data->nb_entity;
}

void		del_entity(t_room *room, t_entity *entity)
{
    t_entity	*tmp;

    if (room->entity != NULL && room->entity->next == NULL)
      room->entity = NULL;
    else if (room->entity == entity)
        room->entity = entity->next;
    else
    {
        tmp = room->entity;
        while (tmp->next != entity)
            tmp = tmp->next;
        if (entity->next != NULL)
            tmp->next = entity->next;
        else
            tmp->next = NULL;
    }
    free(entity);
}
