/*
** sock2.c for server in /home/camill_n/rendu/epic_js_fantasy
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sat May 10 17:27:35 2014 camill_n
** Last update Sat May 10 23:28:53 2014 camill_n
*/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/un.h>
#include "server.h"

void		get_client_paquet(t_data *data, t_client *client)
{
  int		ret;
  char		buff[500];

  while ((ret = read(client->sock, buff, 500)) > 1)
    {
      buff[ret] = 0;
      printf("%s\n", buff);
    }
}

void		check_client(t_data *data, fd_set *rdfs)
{
  t_client	*client;

  client = data->client;
  while (client != NULL)
    {
      printf("sock: %d\n", client->sock);
      if (FD_ISSET(client->sock, rdfs))
	{
	  printf("le client parle\n");
	  get_client_paquet(data, client);
	}
      client = client->next;
    }
}
