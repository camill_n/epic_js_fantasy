/*
** clients.c for server in /home/camill_n/rendu/epic_js_fantasy
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sat May 10 13:01:43 2014 camill_n
** Last update Sat May 10 17:04:13 2014 camill_n
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "server.h"

void		add_client(t_data *data, int sock)
{
    t_client	*tmp;
    t_client	*new_client;

    new_client = malloc(sizeof(t_client));
    new_client->sock = sock;
    new_client->entity = NULL;
    new_client->next = NULL;
    if (data->client == NULL)
      data->client = new_client;
    else
      {
        tmp = data->client;
        while (tmp->next != NULL)
	  tmp = tmp->next;
        tmp->next = new_client;
      }
    ++data->nb_client;
}

void		del_client(t_data *data, t_client *client)
{
    t_client	*tmp;

    if (data->client != NULL && data->client->next == NULL)
      data->client = NULL;
    else if (data->client == client)
        data->client = client->next;
    else
    {
        tmp = data->client;
        while (tmp->next != client)
            tmp = tmp->next;
        if (client->next != NULL)
            tmp->next = client->next;
        else
            tmp->next = NULL;
    }
    --data->nb_client;
    free(client);
}

void	set_entity_to_client(t_data *data, t_client *client, t_entity *entity)
{
  client->entity = entity;
}

t_client	*get_last_client(t_data *data)
{
  t_client	*tmp;

  tmp = data->client;
  while (tmp != NULL && tmp->next != NULL)
    tmp = tmp->next;
  return (tmp);
}
