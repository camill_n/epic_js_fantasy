/*
** add_new_sample.c for server in /home/bousca_a/rendu/epic_js_fantasy
**
** Made by Antonin Bouscarel
** Login   <bousca_a@epitech.net>
**
** Started on  Sat May 10 16:59:27 2014 Antonin Bouscarel
** Last update Sat May 10 18:11:20 2014 Antonin Bouscarel
*/

#include <stdlib.h>
#include <stdio.h>
#include "server.h"

int		add_new_sample_class(t_data *data, char **line)
{
  int		i;

  i = 0;
  while (i < 5)
    {
      if (line[i] == NULL)
	{
	  printf("error sample %s. please check value on sample.bdd\n",
		 line[1]);
	  exit(0);
	}
      i++;
    }
  add_class(data, line[1]);
  add_class_attribute(data, line[1], ATK_SPE, atoi(line[2]));
  add_class_attribute(data, line[1], ATK_SPE_TYPE, atoi(line[3]));
  add_class_attribute(data, line[1], SPE_COST, atoi(line[4]));
  return (0);
}

int		add_new_sample_weapon(t_data *data, char **line)
{
  int		i;

  i = 0;
  while (i < 3)
    {
      if (line[i] == NULL)
	{
	  printf("error sample %s. please check value on sample.bdd",
		 line[1]);
	  exit(0);
	}
      i++;
    }
  add_weapon(data, line[1]);
  add_weapon_attribute(data, line[1], DEG, atoi(line[2]));
  return (0);
}

int		add_new_sample_armor(t_data *data, char **line)
{
  int		i;

  i = 0;
  while (i < 3)
    {
      if (line[i] == NULL)
	{
	  printf("error sample %s. please check value on sample.bdd",
		 line[1]);
	  exit(0);
	}
      i++;
    }
  add_armor(data, line[1]);
  add_armor_attribute(data, line[1], ARMOR_BONUS, atoi(line[2]));
  return (0);
}

int		add_new_sample_adv(t_data *data, char **line)
{
  int		i;

  i = 0;
  while (i < 5)
    {
      if (line[i] == NULL)
	{
	  printf("error sample %s. please check value on sample.bdd",
		 line[1]);
	  exit(0);
	}
      i++;
    }
  add_adv(data, line[1]);
  add_adv_attribute(data, line[1], BONUS_ATK, atoi(line[2]));
  add_adv_attribute(data, line[1], BONUS_SPEED, atoi(line[3]));
  add_adv_attribute(data, line[1], BONUS_SPE, atoi(line[4]));
  return (0);
}
