/*
** action.c for server in /home/bousca_a/rendu/epic_js_fantasy/src
**
** Made by Antonin Bouscarel
** Login   <bousca_a@epitech.net>
**
** Started on  Sat May 10 20:02:19 2014 Antonin Bouscarel
** Last update Sun May 11 00:41:44 2014 camill_n
*/

#include <stdio.h>
#include "server.h"

void		new_basic_battle(t_data *data, char *name_entity_src,
				 char *name_entity_dest, t_room *room)
{
  t_entity	*src;
  t_entity	*dest;

  src = get_entity_addr(data, room, name_entity_src);
  dest = get_entity_addr(data, room, name_entity_dest);
  dest->hp -= src->atk;
  if (dest->hp <= 0)
    del_entity(room, dest);
}

void		new_spe_battle(t_data *data, char *name_entity_src,
			       char *name_entity_dest, t_room *room)
{
  t_entity	*src;
  t_entity	*dest;

  src = get_entity_addr(data, room, name_entity_src);
  dest = get_entity_addr(data, room, name_entity_dest);
  if (src->spe >= src->class->spe_cost)
    {
      if (src->class->atk_spe_type == 0)
	dest->hp -= (src->atk * src->class->atk_spe);
      else if (src->class->atk_spe_type == 1)
	dest->hp += (src->atk * src->class->atk_spe);
      src->spe -= src->class->spe_cost;
    }
  if (dest->hp <= 0)
    del_entity(room, dest);
}

void		change_room(t_data *data, char *name_entity, char *src,
			    char *dest)
{
  t_room	*src_room;
  t_room	*dest_room;
  t_entity	*entity;

  src_room = get_ptr_room(data, src);
  dest_room = get_ptr_room(data, dest);
  entity = get_entity_addr(data, src_room, name_entity);
  set_entity_attr(data, entity, dest_room);
  del_entity(src_room, entity);
}
