/*
** parser_sample.c for server in /home/bousca_a/rendu/epic_js_fantasy/src
**
** Made by Antonin Bouscarel
** Login   <bousca_a@epitech.net>
**
** Started on  Sat May 10 15:13:10 2014 Antonin Bouscarel
** Last update Sat May 10 18:06:23 2014 Antonin Bouscarel
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "server.h"

int		add_data_sample(t_data *data, char *line)
{
  char		**new;

  new = my_wordtab(line, ' ');
  strcmp(new[0], PCLASS) == 0 ? add_new_sample_class(data, new) : 0;
  strcmp(new[0], PWEAPON) == 0 ? add_new_sample_weapon(data, new) : 0;
  strcmp(new[0], PARMOR) == 0 ? add_new_sample_armor(data, new) : 0;
  strcmp(new[0], PADV) == 0 ? add_new_sample_adv(data, new) : 0;
  return (0);
}

void		parse_sample_file(t_data *data)
{
  FILE		*fd;
  char		*line;
  size_t	size;
  int		ret;

  line = NULL;
  size = 512;
  if ((fd = fopen("sample.bdd", "r")) == NULL)
    exit(0);
  while ((ret = getline(&line, &size, fd)) > 0)
    {
      line[ret - 1] = 0;
      if (line[0] == '.')
	add_data_sample(data, line);
      free(line);
      line = NULL;
    }
}
