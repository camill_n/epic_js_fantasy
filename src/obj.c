/*
** obj.c for server in /home/bousca_a/rendu/epic_js_fantasy/src
**
** Made by Antonin Bouscarel
** Login   <bousca_a@epitech.net>
**
** Started on  Sat May 10 14:31:45 2014 Antonin Bouscarel
** Last update Sat May 10 14:41:11 2014 Antonin Bouscarel
*/

#include "server.h"

void		add_weapon_player(t_data *data, char *name_weapon,
				  char *name_entity, t_room *room)
{
  t_entity	*entity;
  t_weapon	*weapon;

  entity = get_entity_addr(data, room, name_entity);
  weapon = get_weapon_ptr(data, name_weapon);
  entity->weapon = weapon;
}

void		add_armor_player(t_data *data, char *name_armor,
				  char *name_entity, t_room *room)
{
  t_entity	*entity;
  t_armor	*armor;

  entity = get_entity_addr(data, room, name_entity);
  armor = get_armor_ptr(data, name_armor);
  entity->armor = armor;
}
