/*
** a.c for server in /home/camill_n/rendu/epic_js_fantasy/src
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sat May 10 01:42:30 2014 camill_n
** Last update Sat May 10 06:34:49 2014 Antonin Bouscarel
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "server.h"

t_armor	*get_armor_ptr(t_data *data, char *armor_name)
{
  t_armor	*tmp;

  tmp = ARMOR;
  while (tmp != NULL && strcmp(tmp->name, armor_name) != 0)
    tmp = tmp->next;
  return (tmp);
}

void		add_armor_attribute(t_data *data, char *armor_name,
				     int macro, int value)
{
  t_armor	*armor;

  armor = get_armor_ptr(data, armor_name);
  macro == ARMOR_BONUS ? armor->bonus = value : 0;
}

void		add_armor(t_data *data, char *name)
{
    t_armor	*tmp;
    t_armor	*new_armor;

    new_armor = malloc(sizeof(t_armor));
    memcpy(new_armor, name, 50);
    new_armor->next = NULL;
    if (data->sample->armor == NULL)
        data->sample->armor = new_armor;
    else
    {
        tmp = data->sample->armor;
        while (tmp->next != NULL)
            tmp = tmp->next;
        tmp->next = new_armor;
    }
}

void		del_armor(t_data *data, t_armor *armor)
{
    t_armor	*tmp;

    if (data->sample->armor != NULL && data->sample->armor->next == NULL)
      data->sample = NULL;
    else if (data->sample->armor == armor)
        data->sample->armor = armor->next;
    else
    {
        tmp = data->sample->armor;
        while (tmp->next != armor)
            tmp = tmp->next;
        if (armor->next != NULL)
            tmp->next = armor->next;
        else
            tmp->next = NULL;
    }
    free(armor);
}
