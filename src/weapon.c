/*
** a.c for server in /home/camill_n/rendu/epic_js_fantasy/src
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sat May 10 01:42:30 2014 camill_n
** Last update Sat May 10 06:02:26 2014 Antonin Bouscarel
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "server.h"

t_weapon	*get_weapon_ptr(t_data *data, char *weapon_name)
{
  t_weapon	*tmp;

  tmp = WEAPON;
  while (tmp != NULL && strcmp(tmp->name, weapon_name) != 0)
    tmp = tmp->next;
  return (tmp);
}

void		add_weapon_attribute(t_data *data, char *weapon_name,
				     int macro, int value)
{
  t_weapon	*weapon;

  weapon = get_weapon_ptr(data, weapon_name);
  macro == DEG ? weapon->deg = value : 0;
}

void		add_weapon(t_data *data, char *name)
{
    t_weapon	*tmp;
    t_weapon	*new_weapon;

    new_weapon = malloc(sizeof(t_weapon));
    memcpy(new_weapon, name, 50);
    new_weapon->next = NULL;
    if (data->sample->weapon == NULL)
        data->sample->weapon = new_weapon;
    else
    {
        tmp = data->sample->weapon;
        while (tmp->next != NULL)
            tmp = tmp->next;
        tmp->next = new_weapon;
    }
}

void		del_weapon(t_data *data, t_weapon *weapon)
{
    t_weapon	*tmp;

    if (data->sample->weapon != NULL && data->sample->weapon->next == NULL)
      data->sample = NULL;
    else if (data->sample->weapon == weapon)
        data->sample->weapon = weapon->next;
    else
    {
        tmp = data->sample->weapon;
        while (tmp->next != weapon)
            tmp = tmp->next;
        if (weapon->next != NULL)
            tmp->next = weapon->next;
        else
            tmp->next = NULL;
    }
    free(weapon);
}
