/*
** sock.c for server in /home/camill_n/rendu/epic_js_fantasy
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sat May 10 13:00:40 2014 camill_n
** Last update Sat May 10 17:59:56 2014 camill_n
*/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/un.h>
#include "server.h"

#define MYPORT 2500
#define BACKLOG 5
#define BUFF_SIZE 100

int	check_socket(int sockfd)
{

  if (listen(sockfd, 1) < 0)
    {
      perror ("listen");
      exit (EXIT_FAILURE);
    }
  return (0);
}

void	init_srv_socket(t_data *data)
{
  int	new_sock;

  bzero(&data->sock_in_srv, sizeof(data->sock_in_srv));
  if ((new_sock = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
      perror("socket");
      exit(-1);
    }
  data->sock_in_srv.sin_family = AF_INET;
  data->sock_in_srv.sin_port = htons(MYPORT);
  data->sock_in_srv.sin_addr.s_addr = INADDR_ANY;
  if (bind(new_sock, (t_sock *)&data->sock_in_srv,
	   sizeof(struct sockaddr)) == -1)
    {
      perror("bind");
      exit(-1);
    }
  check_socket(new_sock);
  data->sock_srv = new_sock;
}

void	create_client(t_data *data, int sock_srv, int *h_sock)
{
  int		new_sock;
  t_sock_in	*buff;
  int		size;
  t_client	*client;

  printf("A new client is now connected!\n");
  buff = malloc(sizeof(t_sock_in));
  new_sock = accept(sock_srv, (t_sock *)buff, (socklen_t *)&size);
  if (new_sock == -1)
    {
      perror("socket");
      exit(-1);
    }
  *h_sock = new_sock;
  add_client(data, new_sock);
  client = get_last_client(data);
  client->sock_in_client.sin_family = buff->sin_family;
  client->sock_in_client.sin_port = htons(buff->sin_port);
  client->sock_in_client.sin_addr = buff->sin_addr;
  client->sock = new_sock;
}

void		check_higher(int *h_sock, t_data *data)
{
  int		ret;
  t_client	*client;

  ret = data->sock_srv;
  client = data->client;
  while (client != NULL)
    {
      if (client->sock > ret)
	ret = client->sock;
      client = client->next;
    }
  *h_sock = ret;
}

int	listen_loop(t_data *data)
{
  fd_set	rdfs;
  int		highest_sock;
  int		check;
  t_client	*client;

  check = 0;
  highest_sock = data->sock_srv;
  init_srv_socket(data);
  printf("Démarage du serveur (PORT: %d)..\n", MYPORT);
  while (!check)
    {
      check_higher(&highest_sock, data);
      FD_ZERO(&rdfs);
      FD_SET(data->sock_srv, &rdfs);
      client = data->client;
      while (client != NULL)
	{
	  FD_SET(client->sock, &rdfs);
	  client = client->next;
	}
      if (select(highest_sock + 1, &rdfs, NULL, NULL, NULL) < 0)
	{
	  perror("select");
	  exit(EXIT_FAILURE);
	}
      if (FD_ISSET(data->sock_srv, &rdfs))
	create_client(data, data->sock_srv, &highest_sock);
      check_client(data, &rdfs);
    }
  close(data->sock_srv);
  return (EXIT_SUCCESS);
}
