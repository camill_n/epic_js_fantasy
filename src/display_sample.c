/*
** display_sample.c for server in /home/bousca_a/rendu/epic_js_fantasy/src
**
** Made by Antonin Bouscarel
** Login   <bousca_a@epitech.net>
**
** Started on  Sat May 10 17:22:39 2014 Antonin Bouscarel
** Last update Sat May 10 18:08:08 2014 Antonin Bouscarel
*/

#include <stdio.h>
#include <stdlib.h>
#include "server.h"

void		display_class(t_data *data)
{
  t_class	*tmp;

  tmp = CLASS;
  while (tmp != NULL)
    {
      printf("Name => %s\n", tmp->name);
      printf("Coef Atk Spe => %d\n", tmp->atk_spe);
      printf("Type Atk Spe => %d\n", tmp->atk_spe_type);
      printf("Atk Spe Speed Cost => %d\n\n", tmp->spe_cost);
      tmp = tmp->next;
    }
}

void		display_armor(t_data *data)
{
  t_armor	*tmp;

  tmp = ARMOR;
  while (tmp != NULL)
    {
      printf("Name => %s\n", tmp->name);
      printf("Bonus HP => %d\n\n", tmp->bonus);
      tmp = tmp->next;
    }
}

void		display_weapon(t_data *data)
{
  t_weapon	*tmp;

  tmp = WEAPON;
  while (tmp != NULL)
    {
      printf("Name => %s\n", tmp->name);
      printf("Bonus Atk => %d\n\n", tmp->deg);
      tmp = tmp->next;
    }
}

void		display_adv(t_data *data)
{
  t_adv		*tmp;

  tmp = ADV;
  while (tmp != NULL)
    {
      printf("Name => %s\n", tmp->name);
      printf("Bonus Atk => %d\n", tmp->bonus_atk);
      printf("Bonus Speed => %d\n", tmp->bonus_speed);
      printf("Bonus Spe => %d\n\n", tmp->bonus_spe);
      tmp = tmp->next;
    }
}
