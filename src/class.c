/*
** a.c for server in /home/camill_n/rendu/epic_js_fantasy/src
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sat May 10 01:42:30 2014 camill_n
** Last update Sat May 10 17:22:27 2014 Antonin Bouscarel
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "server.h"

t_class		*get_class_ptr(t_data *data, char *class_name)
{
  t_class	*tmp;

  tmp = CLASS;
  while (tmp != NULL && strcmp(tmp->name, class_name) != 0)
    tmp = tmp->next;
  return (tmp);
}

void		add_class_attribute(t_data *data, char *class_name, int macro, int value)
{
  t_class	*class;

  class = get_class_ptr(data, class_name);
  macro == ATK_SPE ? class->atk_spe = value : 0;
  macro == ATK_SPE_TYPE ? class->atk_spe_type = value : 0;
  macro == BUFF_SPEED ? class->buff_speed = value : 0;
  macro == BUFF_ATK ? class->buff_deg = value : 0;
  macro == SPE_COST ? class->spe_cost = value : 0;
}

void		add_class(t_data *data, char *name)
{
    t_class	*tmp;
    t_class	*new_class;

    new_class = malloc(sizeof(t_class));
    memcpy(new_class, name, 50);
    new_class->next = NULL;
    if (data->sample->class == NULL)
        data->sample->class = new_class;
    else
    {
        tmp = data->sample->class;
        while (tmp->next != NULL)
            tmp = tmp->next;
        tmp->next = new_class;
    }
}

void		del_class(t_data *data, t_class *class)
{
    t_class	*tmp;

    if (data->sample->class != NULL && data->sample->class->next == NULL)
      data->sample = NULL;
    else if (data->sample->class == class)
        data->sample->class = class->next;
    else
    {
        tmp = data->sample->class;
        while (tmp->next != class)
            tmp = tmp->next;
        if (class->next != NULL)
            tmp->next = class->next;
        else
            tmp->next = NULL;
    }
    free(class);
}
