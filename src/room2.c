/*
** room2.c for server in /home/camill_n/rendu/epic_js_fantasy
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sat May 10 05:08:29 2014 camill_n
** Last update Sat May 10 05:10:11 2014 camill_n
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "server.h"

void	set_avd_room(t_data *data, char *name, int avd)
{
  t_room	*tmp;

  tmp = get_ptr_room(data, name);
  tmp->avd = avd;
}
