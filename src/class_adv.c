/*
** class_adv.c for server in /home/bousca_a/rendu/epic_js_fantasy/src
**
** Made by Antonin Bouscarel
** Login   <bousca_a@epitech.net>
**
** Started on  Sat May 10 15:07:14 2014 Antonin Bouscarel
** Last update Sat May 10 23:43:20 2014 Antonin Bouscarel
*/

#include "server.h"

void		add_adv_to_class(t_data *data, char *name_class, char *adv_name)
{
  t_class	*class;
  t_adv		*adv;

  class = get_class_ptr(data, name_class);
  adv = get_adv_ptr(data, adv_name);
  class->adv = adv;
}

void		add_class_to_entity(t_data *data, char *name_entity,
				    char *name_class, t_room *room)
{
  t_entity	*entity;
  t_class	*class;

  entity = get_entity_addr(data, room, name_entity);
  class = get_class_ptr(data, name_class);
  entity->class = class;
}
