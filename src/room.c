/*
** a.c for server in /home/camill_n/rendu/epic_js_fantasy/src
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sat May 10 01:42:30 2014 camill_n
** Last update Sat May 10 14:05:04 2014 camill_n
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "server.h"

t_room		*get_ptr_room(t_data *data, char *name)
{
  t_room	*tmp;
  int		check;

  check = 0;
  tmp = NULL;
  tmp = data->room;
  check = 0;
  while (tmp != NULL && !check)
    {
      if(strcmp(tmp->name, name) == 0)
	++check;
      else
	tmp = tmp->next;
    }
  return(tmp);
}

void		add_room(t_data *data, char *name)
{
    t_room	*tmp;
    t_room	*new_room;

    new_room = malloc(sizeof(t_room));
    new_room->id_r = data->nb_room;
    new_room->nb_join = 0;
    new_room->join = NULL;
    new_room->entity = NULL;
    new_room->avd = 0;
    bzero((char *)new_room->nb_entity, 2);
    new_room->name = strdup(name);
    new_room->next = NULL;
    if (data->room == NULL)
      data->room = new_room;
    else
    {
        tmp = data->room;
        while (tmp->next != NULL)
            tmp = tmp->next;
        tmp->next = new_room;
    }
    ++data->nb_room;
}

char		*get_room_name(t_data *data, int id_r)
{
  t_room	*tmp;

  tmp = data->room;
  while (tmp != NULL)
    {
      if (tmp->id_r == id_r)
	return (tmp->name);
      tmp = tmp->next;
    }
  return (strdup("NAME UNFOUND\n"));
}

void		add_join(t_data *data, char *name, int id_r)
{
  t_room	*room;

  room = get_ptr_room(data, name);
  room->join = realloc(room->join, (room->nb_join + 1) * sizeof(int));
  room->join[room->nb_join] = id_r;
  ++room->nb_join;
}

void		del_room(t_data *data, t_room *room)
{
    t_room	*tmp;

    if (data->room != NULL && data->room->next == NULL)
      data->room = NULL;
    else if (data->room == room)
        data->room = room->next;
    else
    {
        tmp = data->room;
        while (tmp->next != room)
            tmp = tmp->next;
        if (room->next != NULL)
            tmp->next = room->next;
        else
            tmp->next = NULL;
    }
    free(room);
}
