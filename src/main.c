/*
** main.c for server in /home/camill_n/rendu/epic_js_fantasy
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sat May 10 01:27:51 2014 camill_n
** Last update Sat May 10 23:50:57 2014 Antonin Bouscarel
*/

#include <stdio.h>
#include <stdlib.h>
#include "server.h"

int		main(int ac, char **av)
{
  t_data	data;

  init_data(&data);
  parse_sample_file(&data);
  printf("CLASS\n");
  display_class(&data);
  printf("ADV\n");
  display_adv(&data);
  printf("WEAPON\n");
  display_weapon(&data);
  printf("ARMOR\n");
  display_armor(&data);
  add_room(&data, "test");
  add_entity(&data, "test", 0, "davout");
  add_entity_attributes(&data, "test", HP, 100);
  add_entity_attributes(&data, "test", SPE, 50);
  add_entity_attributes(&data, "test", SPEED, 110);
  add_entity_attributes(&data, "test", ATK, 5);
  add_class_to_entity(&data, "davout", "wizard", data.room);
  add_entity(&data, "test", 0, "monstre 1");
  add_entity_attributes(&data, "test", HP, 100);
  add_entity_attributes(&data, "test", SPE, 50);
  add_entity_attributes(&data, "test", SPEED, 110);
  add_entity_attributes(&data, "test", ATK, 15);
  add_join(&data, "test", 2);
  set_avd_room(&data, "test", 5);
  disp_room(&data);
  new_basic_battle(&data, "davout", "monstre1", data.room);
  printf("______________________________\n");
  disp_room(&data);
  new_spe_battle(&data, "davout", "monstre1", data.room);
  printf("_____________________________\n");
  disp_room(&data);
  listen_loop(&data);
  return (EXIT_SUCCESS);
}
