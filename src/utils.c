/*
** a.c for server in /home/camill_n/rendu/epic_js_fantasy/src
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sat May 10 01:42:30 2014 camill_n
** Last update Sat May 10 23:27:44 2014 Antonin Bouscarel
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "server.h"

void		disp_entity(t_data *data, char *name)
{
  t_room	*room;
  t_entity	*tmp;

  room = get_ptr_room(data, name);
  tmp = room->entity;
  while (tmp != NULL)
    {
      printf("Id_e: %d, Type_e => %d\n", tmp->id_e, tmp->type_e);
      write(1, tmp->name, strlen(tmp->name));
      printf(" HP => %d\nSPE => %d\nSPEED => %d\nATK => %d\n\n",
	     tmp->hp, tmp->spe, tmp->speed, tmp->atk);
      tmp = tmp->next;
    }
}

void		disp_join(t_data *data, char *name)
{
  t_room	*room;
  int		i;

  i = 0;
  room = get_ptr_room(data, name);
  printf("La salle %s est relie a %d salle:\n", name, room->nb_join);
  while (i < room->nb_join)
    {
      printf("Salle: %s\n", get_room_name(data, room->join[i]));
      ++i;
    }
  printf("\n");
}

void		disp_room(t_data *data)
{
  t_room	*tmp;

  tmp = data->room;
  while (tmp != NULL)
    {
      printf("ID => %d\nnom => %s\nAVD: %d\n\n",
	     tmp->id_r, tmp->name, tmp->avd);
      disp_entity(data, tmp->name);
      //      disp_join(data, tmp->name);
      printf("\n");
      tmp = tmp->next;
    }
}

void		disp_client(t_data *data)
{
  t_client	*tmp;

  tmp = data->client;
  printf("Liste des clients connecté (%d client connecté):\n", data->nb_client);
  while (tmp != NULL)
    {
      printf("sock => %d\nrelie a l'entite => %s\n\n",
	     tmp->sock, tmp->entity->name);
      tmp = tmp->next;
    }
  printf("\n");
}
