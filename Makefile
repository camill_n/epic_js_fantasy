##
## Makefile for server in /home/camill_n/rendu/epic_js_fantasy
##
## Made by camill_n
## Login   <camill_n@epitech.net>
##
## Started on  Sat May 10 01:23:33 2014 camill_n
## Last update Sat May 10 20:36:11 2014 Antonin Bouscarel
## Last update Sat May 10 06:35:04 2014 Antonin Bouscarel
##

SRCS =	src/main.c \
	src/gamer.c \
	src/entity.c \
	src/monster.c \
	src/obj.c \
	src/room.c \
	src/utils.c \
	src/init.c \
	src/class.c \
	src/adv.c \
	src/weapon.c \
	src/room2.c \
	src/sock.c \
	src/clients.c \
	src/armor.c \
	src/wordtab.c \
	src/wordtab2.c \
	src/add_new_sample.c \
	src/display_sample.c \
	src/class_adv.c \
	src/parser_sample.c \
	src/sock2.c \
	src/action.c \

RM = 	rm -f

NAME =	js_server

CC =	cc -Wall -g

OBJS =	$(SRCS:.c=.o)

CFLAGS = -I./include/

all:	$(NAME)

.c.o:
	$(CC) $(CFLAGS) -c $< -o $(<:.c=.o)

$(NAME):	$(OBJS)
		$(CC) $(OBJS) -o $(NAME)

clean:
	$(RM) $(OBJS)

fclean:	clean
	$(RM) $(NAME)

re: fclean all
